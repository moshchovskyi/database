import sqlalchemy as db

engine = db.create_engine('sqlite:///new_sqlite.db')
connection = engine.connect()

metadata = db.MetaData()

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'user'
    Id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(255))


class Post(Base):
    __tablename__ = 'post'
    Id = db.Column(db.Integer, primary_key=True)
    Title = db.Column(db.String(255), nullable=False)
    ViewCount = db.Column(db.Integer, default=1000)
    Question = db.Column(db.Boolean, default=True)


Base.metadata.create_all(engine)

from sqlalchemy.orm import sessionmaker

session = sessionmaker()
session.configure(bind=engine)
my_session = session()

Ewa = User(Name = "Ewa")
Adam = User(Name = "Adam")

my_session.add(Ewa)
my_session.add(Adam)

print(my_session.new)
my_session.commit()

one_post = Post(Title="Pierwszy wpis", ViewCount = 200)
one_answer = Post(Title="Pierwsza odpowiedź", Question=False)

my_session.add_all([one_post, one_answer])
my_session.commit()

print(engine.execute("select * from post").fetchall())




