# Try to commit
import pandas as pd
import sqlalchemy as db
import matplotlib.pyplot as plt
import numpy as np

engine = db.create_engine("mysql+mysqlconnector://root:mvv45012@localhost:3306/sqlalchemy_mysql")

connection = engine.connect()

query = 'SELECT * FROM posts'
# result = engine.execute(query).fetchall()
# print(result)

posts_df = pd.read_sql_query(query, engine)

# print(posts_df)
# print(posts_df.columns)
# print(posts_df.head)

# print(posts_df[['ViewCount', 'AnswerCount']])
# print(posts_df[['ViewCount', 'AnswerCount']].min())
# print(posts_df[['ViewCount', 'AnswerCount']].max())
# print(posts_df[['ViewCount', 'AnswerCount']].describe())
# print(posts_df[['ViewCount', 'AnswerCount']].dropna()[:5])

x = posts_df['AnswerCount']
y = posts_df['ViewCount']

colors = np.random.rand(25488)

plt.scatter(x, y, c=colors)
plt.ylim([0, 120000])
plt.title('Posts: Views vs. Answers')
plt.xlabel("Answers")
plt.ylabel("Views")
plt.show()
